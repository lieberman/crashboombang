#!/usr/bin/env bash
clear
echo -e "\033[1;31m     ___ _       ___    _______ \033[0m"
echo -e "\033[1;32m    /   | |     / / |  / / ___/ \033[0m"
echo -e "\033[1;33m   / /| | | /| / /| | / /\__ \\  \033[0m"
echo -e "\033[1;34m  / ___ | |/ |/ / | |/ /___/ /  \033[0m"
echo -e "\033[1;35m /_/  |_|__/|__/  |___//____/   \033[0m"
echo -e "\033[1;36m \033[0m"
echo -e "\033[1;34m -------------- \033[0m"
echo -e "\033[1;31m Thank's fahai && Open Source Enthusiast \n\033[0m"
echo -e "\033[1;32m [ help ] \033[0m"
echo -e "\033[1;35m [ https://www.fahai.org/index.php/archives/146/ ] \033[0m"
echo -e "\033[1;33m [ https://github.com/XRSec/AWVS14-Update ] \n\033[0m"
echo -e "\033[1;34m [ https://awvs.vercel.app/ ] \n\033[0m"

cat /awvs/acunetix/.hosts >> /etc/hosts
cat /etc/hosts | grep acunetix
route del -net default
route add -net default gw 10.255.252.254
su -l acunetix -c /home/acunetix/.acunetix/start.sh
