#!/bin/bash
openssl genrsa -out domain.key 2048
openssl req -new -key domain.key -out domain.csr
openssl x509 -req -years 3 -in domain.csr -signkey domain.key -out domain.crt
openssl x509 -req -days 1000 -in domain.csr -signkey domain.key -out domain.crt
cat domain.key domain.crt > domain.pem
echo
echo 'Seu certificado real está pronto, Senhor!'
echo
